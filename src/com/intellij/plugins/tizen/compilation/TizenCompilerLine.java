package com.intellij.plugins.tizen.compilation;

import com.intellij.openapi.compiler.CompilerMessageCategory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TizenCompilerLine {
  private static final Pattern PACKAGE_FILE_MATCHER = Pattern.compile("^Package File Location: (.*?)$");

  final public CompilerMessageCategory category;
  final public String message;

  final public String packageFilename;

  public TizenCompilerLine(String line) {
    this.message = line;
    if (line.contains("Warning")) {
      this.category = CompilerMessageCategory.WARNING;
    } else if (line.equals("There is no default profiles value in config file.") || line.contains("[Fatal Error]")) {
      this.category = CompilerMessageCategory.ERROR;
    } else {
      this.category = CompilerMessageCategory.INFORMATION;
    }
    final Matcher matcher = PACKAGE_FILE_MATCHER.matcher(message);
    packageFilename = matcher.find() ? matcher.group(1) : null;
  }
}
