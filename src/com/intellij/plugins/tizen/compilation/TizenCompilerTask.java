package com.intellij.plugins.tizen.compilation;

import com.intellij.openapi.compiler.CompileContext;
import com.intellij.openapi.compiler.CompileTask;
import com.intellij.openapi.compiler.FileProcessingCompiler;

public class TizenCompilerTask implements CompileTask {

  static TizenCompiler tizenCompiler;

  @Override
  public boolean execute(CompileContext context) {
    if (tizenCompiler == null) {
      tizenCompiler = new TizenCompiler();
    }
    
    FileProcessingCompiler.ProcessingItem[] processingItems = tizenCompiler.getProcessingItems(context);
    tizenCompiler.process(context, processingItems);
    return true;
  }
}
