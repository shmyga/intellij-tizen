package com.intellij.plugins.tizen.compilation;

import com.intellij.compiler.options.CompileStepBeforeRun;
import com.intellij.execution.ExecutorRegistry;
import com.intellij.execution.configurations.ModuleBasedConfiguration;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.execution.configurations.RunConfigurationModule;
import com.intellij.execution.executors.DefaultDebugExecutor;
import com.intellij.openapi.compiler.*;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.openapi.projectRoots.Sdk;
import com.intellij.openapi.projectRoots.SdkAdditionalData;
import com.intellij.openapi.roots.CompilerModuleExtension;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.roots.OrderEnumerator;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.plugins.tizen.config.sdk.TizenSdkData;
import com.intellij.plugins.tizen.ide.module.TizenModuleSettings;
import com.intellij.plugins.tizen.ide.module.TizenModuleType;
import com.intellij.plugins.tizen.util.TizenCompilerUtil;
import com.intellij.util.PathUtil;
import org.jetbrains.annotations.NotNull;

import java.io.DataInput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TizenCompiler implements SourceProcessingCompiler {
  private static final Logger LOG = Logger.getInstance("#com.intellij.plugins.haxe.compilation.TizenCompiler");

  @NotNull
  public String getDescription() {
    return TizenBundle.message("tizen.compiler.description");
  }

  @Override
  public boolean validateConfiguration(CompileScope scope) {
    return true;
  }

  @NotNull
  @Override
  public ProcessingItem[] getProcessingItems(CompileContext context) {
    final List<ProcessingItem> itemList = new ArrayList<ProcessingItem>();
    for (final Module module : getModulesToCompile(context.getCompileScope() /*, context.getProject() */)) {
      itemList.add(new MyProcessingItem(module));
    }
    return itemList.toArray(new ProcessingItem[itemList.size()]);
  }

  private static List<Module> getModulesToCompile(final CompileScope scope /*, final Project project */) {
    final List<Module> result = new ArrayList<Module>();
    for (final Module module : scope.getAffectedModules()) {
      if (ModuleType.get(module) != TizenModuleType.getInstance()) continue;
      result.add(module);
    }
    return result;
  }

  @Override
  public ProcessingItem[] process(CompileContext context, ProcessingItem[] items) {
    final RunConfiguration runConfiguration = CompileStepBeforeRun.getRunConfiguration(context.getCompileScope());
    if (runConfiguration instanceof ModuleBasedConfiguration) {
      return run(context, items, (ModuleBasedConfiguration)runConfiguration);
    }
    return make(context, items);
  }

  private static ProcessingItem[] run(CompileContext context,
                                      ProcessingItem[] items,
                                      ModuleBasedConfiguration configuration) {
    final Module module = configuration.getConfigurationModule().getModule();
    if (module == null) {
      context.addMessage(CompilerMessageCategory.ERROR,
                         TizenBundle.message("no.module.for.run.configuration", configuration.getName()), null, -1, -1);
      return ProcessingItem.EMPTY_ARRAY;
    }

    TizenCompilerUtil.CompilationContext compilationContext = createCompilationContext(context, module, configuration);

    if (compileModule(context, module, compilationContext)) {
      final int index = findProcessingItemIndexByModule(items, configuration.getConfigurationModule());
      if (index != -1) {
        return new ProcessingItem[]{items[index]};
      }
    }
    return ProcessingItem.EMPTY_ARRAY;
  }

  private static ProcessingItem[] make(CompileContext context, ProcessingItem[] items) {
    final List<ProcessingItem> result = new ArrayList<ProcessingItem>();
    for (ProcessingItem processingItem : items) {
      if (!(processingItem instanceof MyProcessingItem)) {
        continue;
      }
      final MyProcessingItem myProcessingItem = (MyProcessingItem)processingItem;
      if (compileModule(context, myProcessingItem.myModule, createCompilationContext(context, myProcessingItem.myModule, null))) {
        result.add(processingItem);
      }
    }
    return result.toArray(new ProcessingItem[result.size()]);
  }

  private static boolean compileModule(final CompileContext context,
                                       Module module,
                                       @NotNull final TizenCompilerUtil.CompilationContext compilationContext) {
    if (!ModuleUtil.getModuleType(module).equals(TizenModuleType.getInstance())) {
      return true;
    }

    boolean compiled = TizenCompilerUtil.compile(compilationContext);

    if (!compiled) {
      context.addMessage(CompilerMessageCategory.ERROR, "Compilation failed", null, 0, 0);
    }

    return compiled;
  }

  private static int findProcessingItemIndexByModule(ProcessingItem[] items, RunConfigurationModule moduleConfiguration) {
    final Module module = moduleConfiguration.getModule();
    if (module == null || module.getModuleFile() == null) {
      return -1;
    }
    for (int i = 0; i < items.length; ++i) {
      if (module.getModuleFile().equals(items[i].getFile())) {
        return i;
      }
    }
    return -1;
  }

  private static TizenCompilerUtil.CompilationContext createCompilationContext(final CompileContext context,
                                                                                    final Module module,
                                                                                    ModuleBasedConfiguration configuration) {

    final TizenModuleSettings settings = TizenModuleSettings.getInstance(module);
    final ModuleRootManager moduleRootManager = ModuleRootManager.getInstance(module);
    final Sdk sdk = moduleRootManager.getSdk();
    if (sdk == null) {
      context.addMessage(CompilerMessageCategory.ERROR, TizenBundle.message("no.sdk.for.module", module.getName()), null, -1, -1);
      return null;
    }

    return new TizenCompilerUtil.CompilationContext() {

      @NotNull
      @Override
      public TizenModuleSettings getModuleSettings() {
        return settings;
      }

      @Override
      public String getModuleName() {
        return module.getName();
      }

      @Override
      public String getOutputFileName() {
        return null; //getFileNameWithCurrentExtension(getModuleSettings().getOutputFileName());
      }

      @Override
      public void errorHandler(String message) {
        context.addMessage(CompilerMessageCategory.ERROR, message, null, -1, -1);
      }

      @Override
      public void warningHandler(String message) {
        context.addMessage(CompilerMessageCategory.WARNING, message, null, -1, -1);
      }

      @Override
      public void infoHandler(String message) {
        context.addMessage(CompilerMessageCategory.INFORMATION, message, null, -1, -1);
      }

      public void statisticsHandler(String message) {
        context.addMessage(CompilerMessageCategory.INFORMATION, message, null, -1, -1);
      }

      @Override
      public void log(String message) {
        LOG.debug(message);
      }

      @Override
      public String getSdkName() {
        return sdk.getName();
      }

      @Override
      public String getSdkHomePath() {
        return sdk.getHomePath();
      }

      @Override
      public String getTizenPath() {
        SdkAdditionalData data = sdk.getSdkAdditionalData();
        return data instanceof TizenSdkData ? ((TizenSdkData)data).getTizenPath() : null;
      }

      @Override
      public List<String> getSourceRoots() {
        final List<String> result = new ArrayList<String>();
        for (VirtualFile sourceRoot : OrderEnumerator.orderEntries(module).recursively().withoutSdk().exportedOnly().sources().getRoots()) {
          result.add(sourceRoot.getPath());
        }
        for (VirtualFile sourceRoot : OrderEnumerator.orderEntries(module).librariesOnly().getSourceRoots()) {
          result.add(sourceRoot.getPath());
        }
        return result;
      }

      @Override
      public String getCompileOutputPath() {
        final CompilerModuleExtension moduleExtension = CompilerModuleExtension.getInstance(module);
        final String outputUrl = moduleExtension != null ? moduleExtension.getCompilerOutputUrl() : null;
        return VfsUtilCore.urlToPath(outputUrl);
      }

      @Override
      public List<TizenCompilerLine> handleOutput(String[] lines) {
        return TizenCompilerUtil.fillContext(context, lines);
      }

      @Override
      public String getModuleDirPath() {
        return PathUtil.getParentPath(module.getModuleFilePath());
      }
    };
  }

  @Override
  public ValidityState createValidityState(DataInput in) throws IOException {
    return new EmptyValidityState();
  }

  private static class MyProcessingItem implements ProcessingItem {
    private final Module myModule;

    private MyProcessingItem(Module module) {
      myModule = module;
    }

    @NotNull
    public VirtualFile getFile() {
      return myModule.getModuleFile();
    }

    public ValidityState getValidityState() {
      return new EmptyValidityState();
    }
  }
}
