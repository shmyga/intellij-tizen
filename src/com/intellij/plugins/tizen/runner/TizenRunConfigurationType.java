package com.intellij.plugins.tizen.runner;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.extensions.Extensions;
import com.intellij.openapi.project.Project;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.util.containers.ContainerUtil;
import icons.TizenIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class TizenRunConfigurationType implements ConfigurationType {
  private final TizenFactory configurationFactory;

  public TizenRunConfigurationType() {
    configurationFactory = new TizenFactory(this);
  }

  public static TizenRunConfigurationType getInstance() {
    return ContainerUtil.findInstance(Extensions.getExtensions(CONFIGURATION_TYPE_EP), TizenRunConfigurationType.class);
  }

  public String getDisplayName() {
    return TizenBundle.message("runner.configuration.name");
  }

  public String getConfigurationTypeDescription() {
    return TizenBundle.message("runner.configuration.name");
  }

  public Icon getIcon() {
    return TizenIcons.Tizen16;
  }

  @NotNull
  public String getId() {
    return "TizenApplicationRunConfiguration";
  }

  public ConfigurationFactory[] getConfigurationFactories() {
    return new ConfigurationFactory[]{configurationFactory};
  }

  public static class TizenFactory extends ConfigurationFactory {

    public TizenFactory(ConfigurationType type) {
      super(type);
    }

    public RunConfiguration createTemplateConfiguration(Project project) {
      final String name = TizenBundle.message("runner.configuration.name");
      return new TizenApplicationConfiguration(name, project, getInstance());
    }
  }
}
