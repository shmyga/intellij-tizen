package com.intellij.plugins.tizen.runner;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.configurations.RunProfile;
import com.intellij.execution.configurations.RunProfileState;
import com.intellij.execution.executors.DefaultRunExecutor;
import com.intellij.execution.runners.DefaultProgramRunner;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.execution.ui.RunContentDescriptor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.projectRoots.Sdk;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.plugins.tizen.config.sdk.TizenSdkUtil;
import com.intellij.plugins.tizen.device.TizenDevice;
import com.intellij.plugins.tizen.ide.module.TizenModuleSettings;
import com.intellij.plugins.tizen.util.Sdb;
import com.intellij.plugins.tizen.runner.device.DeviceChooserUtil;
import org.jetbrains.annotations.NotNull;


public class TizenRunner extends DefaultProgramRunner {
  public static final String TIZEN_RUNNER_ID = "TizenRunner";

  @NotNull
  @Override
  public String getRunnerId() {
    return TIZEN_RUNNER_ID;
  }

  @Override
  protected RunContentDescriptor doExecute(@NotNull RunProfileState state, @NotNull ExecutionEnvironment env) throws ExecutionException {
    final TizenApplicationConfiguration configuration = (TizenApplicationConfiguration)env.getRunProfile();
    final Module module = configuration.getConfigurationModule().getModule();

    if (module == null) {
      throw new ExecutionException(TizenBundle.message("no.module.for.run.configuration", configuration.getName()));
    }

    final ModuleRootManager moduleRootManager = ModuleRootManager.getInstance(module);
    final TizenModuleSettings settings = TizenModuleSettings.getInstance(module);
    Sdk sdk = moduleRootManager.getSdk();
    if (sdk == null) {
      throw new ExecutionException(TizenBundle.message("no.sdk.for.module", module.getName()));
    }
    String sdbPath = TizenSdkUtil.getTizenPathByFolderPath(sdk.getHomePath());
    String outputFile = settings.getOutputFolder() + "/" + settings.getOutputFileName();
    Sdb sdb = Sdb.getInstance(sdbPath);

    TizenDevice device = DeviceChooserUtil.chooseDevice(module.getProject(), sdb);

    if (device != null) {
      sdb.run(outputFile, device);
    } else {
      throw new ExecutionException("No devices");
    }
    return null;
  }

  @Override
  public boolean canRun(@NotNull String executorId, @NotNull RunProfile profile) {
    return DefaultRunExecutor.EXECUTOR_ID.equals(executorId) && profile instanceof TizenApplicationConfiguration;
  }

}
