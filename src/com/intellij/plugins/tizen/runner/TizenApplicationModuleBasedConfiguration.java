package com.intellij.plugins.tizen.runner;

import com.intellij.execution.configurations.RunConfigurationModule;
import com.intellij.openapi.project.Project;

public class TizenApplicationModuleBasedConfiguration extends RunConfigurationModule {
  public TizenApplicationModuleBasedConfiguration(Project project) {
    super(project);
  }
}
