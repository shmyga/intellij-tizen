package com.intellij.plugins.tizen.runner.device;

import com.intellij.plugins.tizen.device.TizenDevice;
import com.intellij.ui.ColoredTextContainer;
import org.jetbrains.annotations.NotNull;

public interface DeviceNameRendererEx {
  boolean isApplicable(@NotNull TizenDevice device);
  void render(@NotNull TizenDevice device, @NotNull ColoredTextContainer component);
  @NotNull
  String getName(@NotNull TizenDevice device);
}
