package com.intellij.plugins.tizen.runner.device;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.ide.CopyPasteManager;
import com.intellij.openapi.ui.JBPopupMenu;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.plugins.tizen.config.sdk.TizenSdkUtil;
import com.intellij.plugins.tizen.device.TizenDevice;
import com.intellij.plugins.tizen.util.Sdb;
import com.intellij.ui.DoubleClickListener;
import com.intellij.ui.ScrollPaneFactory;
import com.intellij.ui.table.JBTable;
import com.intellij.util.Alarm;
import com.intellij.util.ArrayUtil;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.util.containers.HashSet;
import com.intellij.util.ui.JBUI;
import com.intellij.util.ui.UIUtil;
import com.intellij.util.ui.update.MergingUpdateQueue;
import com.intellij.util.ui.update.Update;
import gnu.trove.TIntArrayList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


public class DeviceChooser implements Disposable {
  private final static int UPDATE_DELAY_MILLIS = 250;
  private static final String[] COLUMN_TITLES = new String[]{"Device", "State", "Compatible", "Serial Number"};
  private static final int DEVICE_NAME_COLUMN_INDEX = 0;
  private static final int DEVICE_STATE_COLUMN_INDEX = 1;
  private static final int COMPATIBILITY_COLUMN_INDEX = 2;
  private static final int SERIAL_COLUMN_INDEX = 3;

  public static final TizenDevice[] EMPTY_DEVICE_ARRAY = new TizenDevice[0];

  private final Sdb mySdb;
  private final List<DeviceChooserListener> myListeners = ContainerUtil.createLockFreeCopyOnWriteList();
  private final MergingUpdateQueue myUpdateQueue;

  private volatile boolean myProcessSelectionFlag = true;

  private final JComponent myPanel;
  private final JBTable myDeviceTable;

  private int[] mySelectedRows;
  private final AtomicBoolean myDevicesDetected = new AtomicBoolean();

  public DeviceChooser(Sdb sdb,
                       boolean multipleSelection,
                       @NotNull final Action okAction) {

    mySdb = sdb;
    myDeviceTable = new JBTable();
    myPanel = ScrollPaneFactory.createScrollPane(myDeviceTable);
    myPanel.setPreferredSize(JBUI.size(550, 220));

    myDeviceTable.setModel(new MyDeviceTableModel(EMPTY_DEVICE_ARRAY));
    myDeviceTable
      .setSelectionMode(multipleSelection ? ListSelectionModel.MULTIPLE_INTERVAL_SELECTION : ListSelectionModel.SINGLE_SELECTION);
    myDeviceTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (myProcessSelectionFlag) {
          fireSelectedDevicesChanged();
        }
      }
    });
    new DoubleClickListener() {
      @Override
      protected boolean onDoubleClick(MouseEvent e) {
        if (myDeviceTable.isEnabled() && okAction.isEnabled()) {
          okAction.actionPerformed(null);
          return true;
        }
        return false;
      }
    }.installOn(myDeviceTable);

    myDeviceTable.setDefaultRenderer(TizenDevice.class, new DeviceRenderer.DeviceNameRenderer());
    myDeviceTable.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER && okAction.isEnabled()) {
          okAction.actionPerformed(null);
        }
      }
    });
    myDeviceTable.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3) {
          int i = myDeviceTable.rowAtPoint(e.getPoint());
          if (i >= 0) {
            Object serial = myDeviceTable.getValueAt(i, SERIAL_COLUMN_INDEX);
            final String serialString = serial.toString();
            // Add a menu to copy the serial key.
            JBPopupMenu popupMenu = new JBPopupMenu();
            Action action = new AbstractAction("Copy Serial Number") {
              @Override
              public void actionPerformed(ActionEvent e) {
                CopyPasteManager.getInstance().setContents(new StringSelection(serialString));
              }
            };
            popupMenu.add(action);
            popupMenu.show(e.getComponent(), e.getX(), e.getY());
          }
        }
        super.mouseReleased(e);
      }
    });
    setColumnWidth(myDeviceTable, DEVICE_NAME_COLUMN_INDEX, "Samsung Galaxy Nexus Android 4.1 (API 17)");
    setColumnWidth(myDeviceTable, DEVICE_STATE_COLUMN_INDEX, "offline");
    setColumnWidth(myDeviceTable, COMPATIBILITY_COLUMN_INDEX, "Compatible");
    setColumnWidth(myDeviceTable, SERIAL_COLUMN_INDEX, "123456");

    // Do not recreate columns on every model update - this should help maintain the column sizes set above
    myDeviceTable.setAutoCreateColumnsFromModel(false);

    // Allow sorting by columns (in lexicographic order)
    myDeviceTable.setAutoCreateRowSorter(true);

    // the device change notifications from adb can sometimes be noisy (esp. when a device is [dis]connected)
    // we use this merging queue to collapse multiple updates to one
    myUpdateQueue = new MergingUpdateQueue("android.device.chooser", UPDATE_DELAY_MILLIS, true, null, this, null,
                                           Alarm.ThreadToUse.POOLED_THREAD);

    //ToDo: add sdb devices listener
  }

  private static void setColumnWidth(JBTable deviceTable, int columnIndex, String sampleText) {
    int width = getWidth(deviceTable, sampleText);
    deviceTable.getColumnModel().getColumn(columnIndex).setPreferredWidth(width);
  }

  private static int getWidth(JBTable deviceTable, String sampleText) {
    FontMetrics metrics = deviceTable.getFontMetrics(deviceTable.getFont());
    return metrics.stringWidth(sampleText);
  }

  public void init(@Nullable String[] selectedSerials) {
    updateTable();
    if (selectedSerials != null) {
      resetSelection(selectedSerials);
    }
  }

  private void resetSelection(@NotNull String[] selectedSerials) {
    MyDeviceTableModel model = (MyDeviceTableModel)myDeviceTable.getModel();
    Set<String> selectedSerialsSet = new HashSet<String>();
    Collections.addAll(selectedSerialsSet, selectedSerials);
    TizenDevice[] myDevices = model.myDevices;
    ListSelectionModel selectionModel = myDeviceTable.getSelectionModel();
    boolean cleared = false;

    for (int i = 0, n = myDevices.length; i < n; i++) {
      String serialNumber = myDevices[i].serialNumber;
      if (selectedSerialsSet.contains(serialNumber)) {
        if (!cleared) {
          selectionModel.clearSelection();
          cleared = true;
        }
        selectionModel.addSelectionInterval(i, i);
      }
    }
  }

  private void updateTable() {
    final TizenDevice[] devices = getDevices();

    UIUtil.invokeLaterIfNeeded(new Runnable() {
      @Override
      public void run() {
        myDevicesDetected.set(devices.length > 0);
        refreshTable(devices);
      }
    });
  }

  private void refreshTable(TizenDevice[] devices) {
    final TizenDevice[] selectedDevices = getSelectedDevices();
    final TIntArrayList selectedRows = new TIntArrayList();
    for (int i = 0; i < devices.length; i++) {
      if (ArrayUtil.indexOf(selectedDevices, devices[i]) >= 0) {
        selectedRows.add(i);
      }
    }

    myProcessSelectionFlag = false;
    myDeviceTable.setModel(new MyDeviceTableModel(devices));
    if (selectedRows.size() == 0 && devices.length > 0) {
      myDeviceTable.getSelectionModel().setSelectionInterval(0, 0);
    }
    for (int selectedRow : selectedRows.toNativeArray()) {
      if (selectedRow < devices.length) {
        myDeviceTable.getSelectionModel().addSelectionInterval(selectedRow, selectedRow);
      }
    }
    fireSelectedDevicesChanged();
    myProcessSelectionFlag = true;
  }

  public boolean hasDevices() {
    return myDevicesDetected.get();
  }

  public JComponent getPreferredFocusComponent() {
    return myDeviceTable;
  }

  @Nullable
  public JComponent getPanel() {
    return myPanel;
  }

  @Nullable
  public ValidationInfo doValidate() {
    if (!myDeviceTable.isEnabled()) {
      return null;
    }

    int[] rows = mySelectedRows != null ? mySelectedRows : myDeviceTable.getSelectedRows();
    boolean hasIncompatible = false;
    boolean hasCompatible = false;
    for (int row : rows) {
      // ToDo:
      hasCompatible = true;
    }
    if (!hasIncompatible) {
      return null;
    }
    String message;
    if (hasCompatible) {
      message = "At least one of the selected devices is incompatible. Will only install on compatible devices.";
    }
    else {
      String devicesAre = rows.length > 1 ? "devices are" : "device is";
      message = "The selected " + devicesAre + " incompatible.";
    }
    return new ValidationInfo(message);
  }

  @NotNull
  public TizenDevice[] getSelectedDevices() {
    int[] rows = mySelectedRows != null ? mySelectedRows : myDeviceTable.getSelectedRows();
    List<TizenDevice> result = new ArrayList<>();
    for (int row : rows) {
      if (row >= 0) {
        Object serial = myDeviceTable.getValueAt(row, SERIAL_COLUMN_INDEX);
        TizenDevice[] devices = getDevices();
        for (TizenDevice device : devices) {
          if (device.serialNumber.equals(serial.toString())) {
            result.add(device);
            break;
          }
        }
      }
    }
    return result.toArray(new TizenDevice[result.size()]);
  }

  @NotNull
  private TizenDevice[] getDevices() {
    return mySdb.getDevices();
  }


  public void finish() {
    mySelectedRows = myDeviceTable.getSelectedRows();
  }

  @Override
  public void dispose() {
    // ToDo: remove listeners
  }

  public void setEnabled(boolean enabled) {
    myDeviceTable.setEnabled(enabled);
  }

  @NotNull
  private static String getDeviceState(@NotNull TizenDevice device) {
    return ""; // ToDo
  }

  private void fireSelectedDevicesChanged() {
    for (DeviceChooserListener listener : myListeners) {
      listener.selectedDevicesChanged();
    }
  }

  public void addListener(@NotNull DeviceChooserListener listener) {
    myListeners.add(listener);
  }

  /*@Override
  public void bridgeChanged(Sdb bridge) {
    postUpdate();
  }*/

  private void postUpdate() {
    myUpdateQueue.queue(new Update("updateTable") {
      @Override
      public void run() {
        updateTable();
      }

      @Override
      public boolean canEat(Update update) {
        return true;
      }
    });
  }

  private class MyDeviceTableModel extends AbstractTableModel {
    private final TizenDevice[] myDevices;

    public MyDeviceTableModel(TizenDevice[] devices) {
      myDevices = devices;
    }

    @Override
    public String getColumnName(int column) {
      return COLUMN_TITLES[column];
    }

    @Override
    public int getRowCount() {
      return myDevices.length;
    }

    @Override
    public int getColumnCount() {
      return COLUMN_TITLES.length;
    }

    @Override
    @Nullable
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (rowIndex >= myDevices.length) {
        return null;
      }
      TizenDevice device = myDevices[rowIndex];
      switch (columnIndex) {
        case DEVICE_NAME_COLUMN_INDEX:
          return device;
        case SERIAL_COLUMN_INDEX:
          return device.serialNumber;
        case DEVICE_STATE_COLUMN_INDEX:
          return getDeviceState(device);
        case COMPATIBILITY_COLUMN_INDEX:
          return true;
      }
      return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      if (columnIndex == COMPATIBILITY_COLUMN_INDEX) {
        return Boolean.class;
      }
      else if (columnIndex == DEVICE_NAME_COLUMN_INDEX) {
        return TizenDevice.class;
      }
      else {
        return String.class;
      }
    }
  }
}
