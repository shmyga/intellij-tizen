package com.intellij.plugins.tizen.runner.device;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.util.Disposer;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.plugins.tizen.device.TizenDevice;
import com.intellij.plugins.tizen.util.Sdb;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class DeviceChooserDialog extends DialogWrapper {
  private final DeviceChooser myDeviceChooser;

  public DeviceChooserDialog(Project project,
                             Sdb sdb,
                             boolean multipleSelection,
                             @Nullable String[] selectedSerials) {
    super(project, true);
    setTitle(TizenBundle.message("choose.device.dialog.title"));

    getOKAction().setEnabled(false);

    myDeviceChooser = new DeviceChooser(sdb, multipleSelection, getOKAction());
    Disposer.register(myDisposable, myDeviceChooser);
    myDeviceChooser.addListener(new DeviceChooserListener() {
      @Override
      public void selectedDevicesChanged() {
        updateOkButton();
      }
    });

    init();
    myDeviceChooser.init(selectedSerials);
  }

  private void updateOkButton() {
    TizenDevice[] devices = getSelectedDevices();
    boolean enabled = devices.length > 0;
    for (TizenDevice device : devices) {
      enabled = true;
    }
    getOKAction().setEnabled(enabled);
  }

  @Override
  public JComponent getPreferredFocusedComponent() {
    return myDeviceChooser.getPreferredFocusComponent();
  }

  @Override
  protected void doOKAction() {
    myDeviceChooser.finish();
    super.doOKAction();
  }

  @Override
  protected String getDimensionServiceKey() {
    return "TizenDeviceChooserDialog";
  }

  @Override
  protected JComponent createCenterPanel() {
    return myDeviceChooser.getPanel();
  }

  public TizenDevice[] getSelectedDevices() {
    return myDeviceChooser.getSelectedDevices();
  }
}
