package com.intellij.plugins.tizen.runner.device;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.plugins.tizen.device.TizenDevice;
import com.intellij.plugins.tizen.util.Sdb;
import org.jetbrains.annotations.Nullable;

public class DeviceChooserUtil {

  @Nullable
  public static TizenDevice chooseDevice(Project project, Sdb sdb) {
    DeviceChooserDialog chooser = new DeviceChooserDialog(project, sdb, false, null);
    chooser.show();
    TizenDevice[] devices = chooser.getSelectedDevices();
    if (chooser.getExitCode() != DialogWrapper.OK_EXIT_CODE || devices.length == 0) {
      return null;
    }
    return devices[0];
  }
}
