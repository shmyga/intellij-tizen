package com.intellij.plugins.tizen.runner.device;

import com.intellij.openapi.extensions.ExtensionPointName;
import com.intellij.plugins.tizen.device.TizenDevice;
import com.intellij.ui.ColoredListCellRenderer;
import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.ColoredTextContainer;
import com.intellij.ui.SimpleTextAttributes;
import icons.TizenIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class DeviceRenderer {

  // Prevent instantiation
  private DeviceRenderer() {
  }

  static void renderDeviceName(TizenDevice device, ColoredTextContainer component) {
    component.setIcon(TizenIcons.Tizen16);
    component.append(device.name, SimpleTextAttributes.REGULAR_ATTRIBUTES);
    component.append(device.serialNumber, SimpleTextAttributes.GRAYED_BOLD_ATTRIBUTES);
  }

  public static class DeviceComboBoxRenderer extends ColoredListCellRenderer {

    @NotNull
    private String myEmptyText;

    public DeviceComboBoxRenderer(@NotNull String emptyText) {
      myEmptyText = emptyText;
    }

    public DeviceComboBoxRenderer() {
      this("[none]");
    }

    @Override
    protected void customizeCellRenderer(JList list, Object value, int index, boolean selected, boolean hasFocus) {
      if (value instanceof String) {
        append((String)value, SimpleTextAttributes.ERROR_ATTRIBUTES);
      }
      else if (value instanceof TizenDevice) {
        renderDeviceName((TizenDevice)value, this);
      }
      else if (value == null) {
        append(myEmptyText, SimpleTextAttributes.ERROR_ATTRIBUTES);
      }
    }
  }

  public static class DeviceNameRenderer extends ColoredTableCellRenderer {
    private static final ExtensionPointName<DeviceNameRendererEx> EP_NAME = ExtensionPointName.create("com.intellij.tizen.run.deviceNameRenderer");
    private final DeviceNameRendererEx[] myRenderers = EP_NAME.getExtensions();

    public DeviceNameRenderer() {
    }

    @Override
    protected void customizeCellRenderer(JTable table, Object value, boolean selected, boolean hasFocus, int row, int column) {
      if (!(value instanceof TizenDevice)) {
        return;
      }

      TizenDevice device = (TizenDevice)value;
      for (DeviceNameRendererEx renderer : myRenderers) {
        if (renderer.isApplicable(device)) {
          renderer.render(device, this);
          return;
        }
      }

      renderDeviceName(device, this);
    }
  }
}
