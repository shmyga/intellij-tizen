package com.intellij.plugins.tizen.util;


import com.intellij.execution.ExecutionException;
import com.intellij.plugins.tizen.device.TizenDevice;
import org.tizen.sdblib.IDevice;
import org.tizen.sdblib.SmartDevelopmentBridge;
import org.tizen.web.tv.sec.nacl.adapter.launch.Emulator;
import org.tizen.web.zimlaunch.util.ConnectionUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Sdb {

  private String path;
  private SmartDevelopmentBridge bridge;
  private IDevice[] devices;
  private Map<String, IDevice> devicesBySerialNumber;

  private static Map<String, Sdb> instances = new HashMap<>();

  public static Sdb getInstance(String path) {
    if (!instances.containsKey(path)) {
      instances.put(path, new Sdb(path));
    }
    return instances.get(path);
  }

  public Sdb(String path) {
    this.path = path;
    this.devicesBySerialNumber = new HashMap<>();
  }

  private SmartDevelopmentBridge resolveBridge() {
    if (bridge == null) {
      bridge = SmartDevelopmentBridge.createBridge(path);
      //bridge.setSdbLogLocation("log");
      bridge.startBridge();
      try {
        Thread.sleep(1000);
      } catch (InterruptedException ignored) {}
      if (!bridge.isConnected()) {
        bridge.restart();
        try {
          Thread.sleep(1000);
        } catch (InterruptedException ignored) {}
      }
    }
    return bridge;
  }

  public TizenDevice[] getDevices() {
    devicesBySerialNumber = new HashMap<>();
    SmartDevelopmentBridge bridge = resolveBridge();
    devices = bridge.getDevices();
    TizenDevice[] result = new TizenDevice[devices.length];
    for (int i = 0; i < devices.length; i++) {
      IDevice device = devices[i];
      devicesBySerialNumber.put(device.getSerialNumber(), device);
      result[i] = new TizenDevice(device.getDeviceName(), device.getSerialNumber());
    }
    return result;
  }

  public void run(String widget, TizenDevice tizenDevice) throws ExecutionException {
    if (!devicesBySerialNumber.containsKey(tizenDevice.serialNumber)) {
      getDevices();
    }
    if (!devicesBySerialNumber.containsKey(tizenDevice.serialNumber)) {
      throw new ExecutionException("No device: " + tizenDevice.serialNumber);
    }
    IDevice device = devicesBySerialNumber.get(tizenDevice.serialNumber);
    ConnectionUtil.getInstance().setSelectedDevice(device);
    System.out.println("Device: " + device);
    try {
      Emulator emulator = new Emulator(tizenDevice.serialNumber);
      emulator.setDebugMode(true);
      System.out.println("Emulator: " + emulator);
      File widgetFile = new File(widget);
      if (widgetFile.exists()) {
        emulator.install(widgetFile, System.out);
        emulator.launch(widgetFile, System.out);
      } else {
        throw new ExecutionException("No widget file: " + widgetFile.getAbsolutePath());
      }
    } catch (Exception e) {
      throw new ExecutionException(e.getMessage());
    }
  }
}
