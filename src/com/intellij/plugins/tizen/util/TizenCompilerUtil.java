package com.intellij.plugins.tizen.util;

import com.intellij.execution.process.BaseOSProcessHandler;
import com.intellij.execution.process.ColoredProcessHandler;
import com.intellij.execution.process.ProcessAdapter;
import com.intellij.execution.process.ProcessEvent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.compiler.CompileContext;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowId;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.plugins.tizen.compilation.TizenCompilerLine;
import com.intellij.plugins.tizen.config.sdk.TizenSdkUtil;
import com.intellij.plugins.tizen.ide.module.TizenModuleSettings;
import com.intellij.util.BooleanValueHolder;
import com.intellij.openapi.compiler.CompilerMessageCategory;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TizenCompilerUtil {
  public interface CompilationContext {
    @NotNull
    TizenModuleSettings getModuleSettings();

    String getModuleName();

    String getOutputFileName();

    void errorHandler(String message);
    void warningHandler(String message);
    void infoHandler(String message);

    void log(String message);

    String getSdkHomePath();

    public String getTizenPath();

    String getSdkName();

    List<String> getSourceRoots();

    String getCompileOutputPath();

    List<TizenCompilerLine> handleOutput(String[] lines);

    String getModuleDirPath();
  }

  public static boolean compile(final CompilationContext context) {
    TizenModuleSettings settings = context.getModuleSettings();

    if (context.getSdkHomePath() == null) {
      context.errorHandler(TizenBundle.message("no.sdk.for.module", context.getModuleName()));
      return false;
    }

    final String tizenPath = TizenSdkUtil.getTizenPathByFolderPath(context.getSdkHomePath());

    if (tizenPath == null || tizenPath.isEmpty()) {
      context.errorHandler(TizenBundle.message("invalid.sdk.for.module", context.getModuleName()));
      return false;
    }

    if (settings.getOutputFileName().isEmpty()) {
      context.errorHandler(TizenBundle.message("invalid.settings.output_file"));
      return false;
    }

    final List<String> commandLine = new ArrayList<String>();

    commandLine.add(tizenPath);

    String workingPath = context.getCompileOutputPath() + "/app";
    String configFile = settings.getConfigFile();
    String outputFile = settings.getOutputFolder() + "/" + settings.getOutputFileName();

    try {
      FileUtil.copy(new File(configFile), new File(workingPath + "/config.xml"));
      for (String sourceRoot : context.getSourceRoots()) {
        FileUtil.copyDir(new File(sourceRoot), new File(workingPath));
      }
    } catch (IOException e) {
      context.errorHandler("Process throw exception: " + e.getMessage());
      return false;
    }

    setupTizen(commandLine, context);

    // Show the command line in the output window.
    if (!commandLine.isEmpty()) {
      StringBuilder cl = new StringBuilder("Executing: ");
      for (String commandPart : commandLine) {
        cl.append(commandPart);
        cl.append(" ");
      }
      context.infoHandler(cl.toString());
    }

    final BooleanValueHolder hasErrors = new BooleanValueHolder(false);

    try {
      final File workingDirectory = new File(FileUtil.toSystemDependentName(workingPath));
      if (!workingDirectory.exists()) {
        if (!workingDirectory.mkdirs()) throw new IOException("Cannot create directory " + workingPath);
      }
      final BaseOSProcessHandler handler = new ColoredProcessHandler(
        new ProcessBuilder(commandLine).directory(workingDirectory).start(),
        commandLine.get(0),
        Charset.defaultCharset()
      );

      handler.addProcessListener(new ProcessAdapter() {
        @Override
        public void onTextAvailable(ProcessEvent event, Key outputType) {
          String[] lines = event.getText().split("\\n");
          for (TizenCompilerLine line : context.handleOutput(lines)) {
            switch (line.category) {
              case INFORMATION:
                context.infoHandler(line.message);
                if (line.packageFilename != null) {
                  try {
                    // ToDo: move
                    FileUtil.copy(new File(line.packageFilename), new File(outputFile));
                    FileUtil.delete(new File(line.packageFilename));
                  } catch (IOException e) {
                    context.errorHandler("Process throw exception: " + e.getMessage());
                    hasErrors.setValue(true);
                  }
                }
                break;
              case WARNING:
                context.warningHandler(line.message);
                break;
              case ERROR:
                context.errorHandler(line.message);
                hasErrors.setValue(true);
                break;
            }
          }
        }

        @Override
        public void processTerminated(ProcessEvent event) {
          hasErrors.setValue(event.getExitCode() != 0);
          super.processTerminated(event);
        }
      });

      handler.startNotify();
      handler.waitFor();
    }
    catch (IOException e) {
      context.errorHandler("Process throw exception: " + e.getMessage());
      return false;
    }

    return !hasErrors.getValue();
  }

  private static void setupTizen(List<String> commandLine, CompilationContext context) {
    final TizenModuleSettings settings = context.getModuleSettings();
    commandLine.add("package");
    commandLine.add("-t");
    commandLine.add("wgt");
    String arguments = settings.getArguments();
    if (arguments != null && !arguments.isEmpty()) {
      Collections.addAll(commandLine, arguments.split(" "));
    }
  }

  public static List<TizenCompilerLine> fillContext(CompileContext context, String[] lines)
  {
    List<TizenCompilerLine> result = new ArrayList<>();
    for (String line : lines) {
      result.add(addLineToContext(line, context));
    }
    return result;
  }

  private static TizenCompilerLine addLineToContext(String line, CompileContext context)
  {
    // TODO: Add a button to the Haxe module settings to control whether we always open the window or not.
    if (context.getUserData(messageWindowAutoOpened) == null) {
      openCompilerMessagesWindow(context);
      context.putUserData(messageWindowAutoOpened, "yes");
    }
    return new TizenCompilerLine(line);
  }

  private static boolean isHeadless() {
    return ApplicationManager.getApplication().isUnitTestMode() || ApplicationManager.getApplication().isHeadlessEnvironment();
  }

  private static void openCompilerMessagesWindow(final CompileContext context) {
    // Force the compile window open.  We should probably have a configuration button
    // on the compile gui page to force it open or not.
    if (!isHeadless()) {
      ApplicationManager.getApplication().invokeLater(new Runnable() {
        public void run() {
          // This was lifted from intellij-community/java/compiler/impl/src/com/intellij/compiler/progress/CompilerTask.java
          final ToolWindow tw = ToolWindowManager.getInstance(context.getProject()).getToolWindow(ToolWindowId.MESSAGES_WINDOW);
          if (tw != null) {
            tw.activate(null, false);
          }
        }
      });
    }
  }

  private static com.intellij.openapi.util.Key messageWindowAutoOpened =
      new com.intellij.openapi.util.Key("messageWindowAutoOpened");
}
