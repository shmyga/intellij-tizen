package com.intellij.plugins.tizen.device;

public class TizenDevice {

  public final String name;
  public final String serialNumber;

  public TizenDevice(String name, String serialNumber) {
    this.name = name;
    this.serialNumber = serialNumber;
  }
}
