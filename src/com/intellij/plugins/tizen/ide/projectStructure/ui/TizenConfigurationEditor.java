package com.intellij.plugins.tizen.ide.projectStructure.ui;

import com.intellij.ide.util.TreeFileChooser;
import com.intellij.ide.util.TreeFileChooserFactory;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.UnnamedConfigurable;
import com.intellij.openapi.roots.CompilerModuleExtension;
import com.intellij.openapi.roots.impl.DirectoryIndex;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.plugins.tizen.ide.module.TizenModuleSettings;
import com.intellij.psi.PsiFile;
import com.intellij.ui.RawCommandLineEditor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


public class TizenConfigurationEditor {
  private JPanel myMainPanel;
  private JTextField myOutputFileNameTextField;
  private TextFieldWithBrowseButton myFolderTextField;
  private JLabel myFolderLabel;
  private JPanel myCompilerOptions;
  private RawCommandLineEditor myArguments;
  private JPanel myBuildFilePanel;
  private JPanel myCompilerOptionsWrapper;
  private TextFieldWithBrowseButton myConfigChooserTextField;
  private JPanel myBuildPanel;

  private final Module myModule;
  private final CompilerModuleExtension myExtension;

  private final List<UnnamedConfigurable> configurables = new ArrayList<UnnamedConfigurable>();

  public TizenConfigurationEditor(Module module, CompilerModuleExtension extension) {
    myModule = module;
    myExtension = extension;
    addActionListeners();
    myFolderLabel.setLabelFor(myFolderTextField.getTextField());
  }

  private void addActionListeners() {
    myConfigChooserTextField.getButton().addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        TreeFileChooser fileChooser = TreeFileChooserFactory.getInstance(myModule.getProject()).createFileChooser(
          TizenBundle.message("tizen.settings.config_file"),
          null,
          null,
          new TreeFileChooser.PsiFileFilter() {
            public boolean accept(PsiFile file) {
              return true;
            }
          });

        fileChooser.showDialog();

        PsiFile selectedFile = fileChooser.getSelectedFile();
        if (selectedFile != null) {
          myConfigChooserTextField.setText(FileUtil.toSystemDependentName(selectedFile.getVirtualFile().getPath()));
        }
      }
    });

    myFolderTextField.getButton().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        final VirtualFile folder =
          FileChooser.chooseFile(FileChooserDescriptorFactory.createSingleFolderDescriptor(), myModule.getProject(), null);
        if (folder != null) {
          myFolderTextField.setText(FileUtil.toSystemDependentName(folder.getPath()));
        }
      }
    });
  }

  public boolean isModified() {
    final TizenModuleSettings settings = TizenModuleSettings.getInstance(myModule);
    assert settings != null;

    final String url = myExtension.getCompilerOutputUrl();
    final String urlCandidate = VfsUtilCore.pathToUrl(myFolderTextField.getText());
    boolean result = !urlCandidate.equals(url);

    result = result || !settings.getConfigFile().equals(myConfigChooserTextField.getText());
    result = result || !settings.getArguments().equals(myArguments.getText());
    result = result || !settings.getOutputFileName().equals(myOutputFileNameTextField.getText());
    result = result || !settings.getOutputFolder().equals(myFolderTextField.getText());

    for (UnnamedConfigurable configurable : configurables) {
      result = result || configurable.isModified();
    }

    return result;
  }

  public void reset() {
    final TizenModuleSettings settings = TizenModuleSettings.getInstance(myModule);
    assert settings != null;

    myConfigChooserTextField.setText(settings.getConfigFile());
    myOutputFileNameTextField.setText(settings.getOutputFileName());
    myFolderTextField.setText(settings.getOutputFolder());
    for (UnnamedConfigurable configurable : configurables) {
      configurable.reset();
    }

    final String url = myExtension.getCompilerOutputUrl();
    myFolderTextField.setText(VfsUtil.urlToPath(url));
    myArguments.setText(settings.getArguments());
  }

  public void apply() {
    final TizenModuleSettings settings = TizenModuleSettings.getInstance(myModule);
    assert settings != null;
    settings.setConfigFile(myConfigChooserTextField.getText());
    settings.setArguments(myArguments.getText());
    settings.setOutputFileName(myOutputFileNameTextField.getText());
    settings.setOutputFolder(myFolderTextField.getText());

    for (UnnamedConfigurable configurable : configurables) {
      try {
        configurable.apply();
      }
      catch (ConfigurationException ignored) {
      }
    }

    final String url = myExtension.getCompilerOutputUrl();
    final String urlCandidate = VfsUtil.pathToUrl(myFolderTextField.getText());

    if (!urlCandidate.equals(url)) {
      myExtension.setCompilerOutputPath(urlCandidate);
      myExtension.commit();
    }
  }

  public JComponent getMainPanel() {
    return myMainPanel;
  }
}
