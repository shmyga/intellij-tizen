package com.intellij.plugins.tizen.ide.projectStructure;

import com.intellij.openapi.module.ModuleConfigurationEditor;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.roots.CompilerModuleExtension;
import com.intellij.openapi.roots.ui.configuration.ModuleConfigurationState;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.plugins.tizen.ide.projectStructure.ui.TizenConfigurationEditor;
import org.jetbrains.annotations.Nls;

import javax.swing.*;


public class TizenModuleConfigurationEditor implements ModuleConfigurationEditor {
  private final TizenConfigurationEditor haxeConfigurationEditor;

  public TizenModuleConfigurationEditor(ModuleConfigurationState state) {
    haxeConfigurationEditor = new TizenConfigurationEditor(state.getRootModel().getModule(), state.getRootModel().getModuleExtension(
      CompilerModuleExtension.class));
  }

  @Override
  public void saveData() {
  }

  @Override
  public void moduleStateChanged() {
  }

  @Nls
  @Override
  public String getDisplayName() {
    return TizenBundle.message("tizen.module.editor.tizen");
  }

  @Override
  public String getHelpTopic() {
    return null;
  }

  @Override
  public JComponent createComponent() {
    return haxeConfigurationEditor.getMainPanel();
  }

  @Override
  public boolean isModified() {
    return haxeConfigurationEditor.isModified();
  }

  @Override
  public void apply() throws ConfigurationException {
    haxeConfigurationEditor.apply();
  }

  @Override
  public void reset() {
    haxeConfigurationEditor.reset();
  }

  @Override
  public void disposeUIResources() {
  }
}
