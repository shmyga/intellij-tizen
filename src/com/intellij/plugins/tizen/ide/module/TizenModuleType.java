package com.intellij.plugins.tizen.ide.module;

import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.ide.util.projectWizard.ProjectJdkForModuleStep;
import com.intellij.ide.util.projectWizard.WizardContext;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.module.ModuleTypeManager;
import com.intellij.openapi.roots.ui.configuration.ModulesProvider;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.plugins.tizen.config.sdk.TizenSdkType;
import icons.TizenIcons;

import javax.swing.*;


public class TizenModuleType extends ModuleType {
  private static final String MODULE_TYPE_ID = "TIZEN_MODULE";

  public TizenModuleType() {
    super(MODULE_TYPE_ID);
  }

  public static TizenModuleType getInstance() {
    return (TizenModuleType) ModuleTypeManager.getInstance().findByID(MODULE_TYPE_ID);
  }

  @Override
  public String getName() {
    return TizenBundle.message("tizen.module.type.name");
  }

  @Override
  public String getDescription() {
    return TizenBundle.message("tizen.module.type.description");
  }

  @Override
  public Icon getBigIcon() {
    return TizenIcons.Tizen128;
  }

  @Override
  public Icon getNodeIcon(boolean isOpened) {
    return TizenIcons.Tizen16;
  }

  @Override
  public TizenModuleBuilder createModuleBuilder() {
    return new TizenModuleBuilder();
  }


  public ModuleWizardStep[] createWizardSteps(final WizardContext wizardContext,
                                              final TizenModuleBuilder moduleBuilder,
                                              final ModulesProvider modulesProvider) {
    return new ModuleWizardStep[]{new ProjectJdkForModuleStep(wizardContext, TizenSdkType.getInstance()) {
      public void updateDataModel() {
        super.updateDataModel();
        moduleBuilder.setModuleJdk(getJdk());
        // TODO: generate Main class as entry point for new projects
      }
    }};
  }
}