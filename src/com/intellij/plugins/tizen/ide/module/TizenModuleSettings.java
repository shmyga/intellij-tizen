package com.intellij.plugins.tizen.ide.module;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleServiceManager;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;

@State(
  name = "TizenModuleSettingsStorage",
  storages = {
    @Storage(
      file = "$MODULE_FILE$"
    )
  }
)

public class TizenModuleSettings implements PersistentStateComponent<TizenModuleSettings> {

  protected String configFile = "";
  protected String arguments = "";
  protected String outputFileName = "";
  protected String outputFolder = "";

  public TizenModuleSettings() {}

  public TizenModuleSettings(String configFile, String arguments, String outputFileName, String outputFolder) {
    this.configFile = configFile;
    this.arguments = arguments;
    this.outputFileName = outputFileName;
    this.outputFolder = outputFolder;
  }

  @Override
  public TizenModuleSettings getState() {
    return this;
  }

  @Override
  public void loadState(TizenModuleSettings state) {
    XmlSerializerUtil.copyBean(state, this);
  }

  public String getConfigFile() {
    return configFile;
  }

  public void setConfigFile(String configFile) {
    this.configFile = configFile;
  }

  public String getArguments() {
    return arguments;
  }

  public void setArguments(String arguments) {
    this.arguments = arguments;
  }

  public String getOutputFileName() {
    return outputFileName;
  }

  public void setOutputFileName(String outputFileName) {
    this.outputFileName = outputFileName;
  }

  public String getOutputFolder() {
    return outputFolder;
  }

  public void setOutputFolder(String outputFolder) {
    this.outputFolder = outputFolder;
  }

  public static TizenModuleSettings getInstance(@NotNull Module module) {
    return ModuleServiceManager.getService(module, TizenModuleSettings.class);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    TizenModuleSettings settings = (TizenModuleSettings)o;

    if (configFile != null ? !configFile.equals(settings.configFile) : settings.configFile != null) return false;
    if (arguments != null ? !arguments.equals(settings.arguments) : settings.arguments != null) return false;
    if (outputFileName != null ? !outputFileName.equals(settings.outputFileName) : settings.outputFileName != null) return false;
    if (outputFolder!= null ? !outputFolder.equals(settings.outputFolder) : settings.outputFolder!= null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = configFile != null ? configFile.hashCode() : 0;
    result = 31 * result + (outputFileName != null ? outputFileName.hashCode() : 0);
    result = 31 * result + (outputFolder != null ? outputFolder.hashCode() : 0);
    result = 31 * result + (arguments != null ? arguments.hashCode() : 0);
    return result;
  }
}
