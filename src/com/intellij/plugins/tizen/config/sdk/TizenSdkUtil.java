package com.intellij.plugins.tizen.config.sdk;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.projectRoots.SdkModificator;
import com.intellij.openapi.util.SystemInfo;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TizenSdkUtil {
  private static enum FileType {
    NORMAL,
    EXECUTABLE,
    SCRIPT,
  }

  private static final Logger LOG = Logger.getInstance("#com.intellij.plugins.haxe.config.sdk.TizenSdkUtil");
  private static final Pattern VERSION_MATCHER = Pattern.compile("^TIZEN_SDK_VERSION=(.*?)$");

  protected static final String SDB_NAME = "sdb";
  protected static final String TIZEN_NAME = "tizen";
  protected static final String VERSION_NAME = "sdk.version";

  @Nullable
  public static String getTizenPathByFolderPath(String folderPath) {
    return getExecutablePathByFolderPath(folderPath + "/tools/ide/bin", TIZEN_NAME, FileType.SCRIPT);
  }

  @Nullable
  public static String getSdbPathByFolderPath(String folderPath) {
    return getExecutablePathByFolderPath(folderPath + "/tools", SDB_NAME, FileType.EXECUTABLE);
  }

  @Nullable
  public static String getVersionPathByFolderPath(String folderPath) {
    return getExecutablePathByFolderPath(folderPath, VERSION_NAME, FileType.NORMAL);
  }

  @Nullable
  private static String getExecutablePathByFolderPath(String folderPath, String name, FileType type) {
    final String resultPath = folderPath + "/" + getExecutableName(name, type);
    LOG.debug("resultPath: " + resultPath);
    if (fileExists(resultPath)) {
      return FileUtil.toSystemIndependentName(resultPath);
    }

    LOG.debug(name + " path: null");
    return null;
  }

  public static String getExecutableName(String name, FileType type) {
    switch (type) {
      case NORMAL:
        return name;
      case EXECUTABLE:
        if (SystemInfo.isWindows) {
          return name + ".exe";
        }
        return name;
      case SCRIPT:
        if (SystemInfo.isWindows) {
          return name + ".bat";
        } else {
          return name + ".sh";
        }
    }
    return name;
  }

  private static boolean fileExists(@Nullable String filePath) {
    return filePath != null && new File(FileUtil.toSystemDependentName(filePath)).exists();
  }
  @Nullable
  public static TizenSdkData testTizenSdk(String path) {
    final String sdbPath = getSdbPathByFolderPath(path);
    if (sdbPath == null) {
      return null;
    }

    final String tizenPath = getTizenPathByFolderPath(path);
    if (tizenPath == null) {
      return null;
    }

    String tizenVersion = "NA";
    final String versionPath = getVersionPathByFolderPath(path);
    if (versionPath != null) {
      try {
        String data = FileUtil.loadFile(new File(versionPath));
        final Matcher matcher = VERSION_MATCHER.matcher(data);
        if (matcher.find()) {
          tizenVersion = matcher.group(1);
        }
      } catch (IOException e) {
        LOG.warn("version", e);
      }
    }

    final TizenSdkData tizenSdkData = new TizenSdkData(path, sdbPath, tizenPath, tizenVersion);
    return tizenSdkData;
  }

  public static void setupSdkPaths(@Nullable VirtualFile sdkRoot, SdkModificator modificator) {

  }

  @Nullable
  public static String suggestHomePath() {
    return null;
  }
}
