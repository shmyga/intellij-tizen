package com.intellij.plugins.tizen.config.sdk;

import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.projectRoots.*;
import com.intellij.openapi.roots.JavadocOrderRootType;
import com.intellij.openapi.roots.OrderRootType;
import com.intellij.openapi.util.SystemInfo;
import com.intellij.plugins.tizen.TizenBundle;
import com.intellij.util.xmlb.XmlSerializer;
import icons.TizenIcons;
import org.jdom.Element;

import javax.swing.*;

public class TizenSdkType extends SdkType {
  
  public TizenSdkType() {
    super(TizenBundle.message("tizen.sdk.name"));
  }

  @Override
  public Icon getIcon() {
    return TizenIcons.Tizen16;
  }

  @Override
  public Icon getIconForAddAction() {
    return TizenIcons.Tizen16;
  }

  public static TizenSdkType getInstance() {
    return SdkType.findInstance(TizenSdkType.class);
  }

  @Override
  public String getPresentableName() {
    return TizenBundle.message("tizen.sdk.name.presentable");
  }

  @Override
  public String suggestSdkName(String currentSdkName, String sdkHome) {
    return TizenBundle.message("tizen.sdk.name.suggest", getVersionString(sdkHome));
  }

  @Override
  public String getVersionString(String sdkHome) {
    final TizenSdkData tizenSdkData = TizenSdkUtil.testTizenSdk(sdkHome);
    return tizenSdkData != null ? tizenSdkData.getVersion() : super.getVersionString(sdkHome);
  }

  @Override
  public String suggestHomePath() {
    return TizenSdkUtil.suggestHomePath();
  }

  @Override
  public boolean isValidSdkHome(String path) {
    return TizenSdkUtil.testTizenSdk(path) != null;
  }

  @Override
  public AdditionalDataConfigurable createAdditionalDataConfigurable(SdkModel sdkModel, SdkModificator sdkModificator) {
    //return new TizenAdditionalConfigurable();
    return null;
  }

  @Override
  public boolean isRootTypeApplicable(OrderRootType type) {
    return type == OrderRootType.SOURCES || type == OrderRootType.CLASSES || type == JavadocOrderRootType.getInstance();
  }

  @Override
  public void setupSdkPaths(Sdk sdk) {
    final SdkModificator modificator = sdk.getSdkModificator();

    SdkAdditionalData data = sdk.getSdkAdditionalData();
    if (data == null) {
      data = TizenSdkUtil.testTizenSdk(sdk.getHomePath());
      modificator.setSdkAdditionalData(data);
    }

    TizenSdkUtil.setupSdkPaths(sdk.getHomeDirectory(), modificator);

    modificator.commitChanges();
    super.setupSdkPaths(sdk);
  }

  @Override
  public SdkAdditionalData loadAdditionalData(Element additional) {
    return XmlSerializer.deserialize(additional, TizenSdkData.class);
  }

  @Override
  public void saveAdditionalData(SdkAdditionalData additionalData, Element additional) {
    if (additionalData instanceof TizenSdkData) {
      XmlSerializer.serializeInto(additionalData, additional);
    }
  }

  @Override
  public FileChooserDescriptor getHomeChooserDescriptor() {
    final FileChooserDescriptor result = super.getHomeChooserDescriptor();
    if (SystemInfo.isMac) {
      result.withShowHiddenFiles(true); // TODO: Test on a mac: converted for v15.
    }
    return result;
  }
}