/*
 * Copyright 2000-2013 JetBrains s.r.o.
 * Copyright 2014-2014 AS3Boyan
 * Copyright 2014-2014 Elias Ku
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.intellij.plugins.tizen.config.sdk;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.projectRoots.SdkAdditionalData;
import com.intellij.util.xmlb.XmlSerializerUtil;

public class TizenSdkData implements SdkAdditionalData, PersistentStateComponent<TizenSdkData> {

  private String homePath = "";
  private String sdbPath = "";
  private String tizenPath = "";
  private String version = "";

  public TizenSdkData() {}

  public TizenSdkData(String homePath, String sdbPath, String tizenPath, String version) {
    this.homePath = null == homePath ? "" : homePath;
    this.sdbPath = null == sdbPath ? "" : sdbPath;
    this.tizenPath = null == tizenPath ? "" : tizenPath;
    this.version  = null == version  ? "" : version;
  }

  @SuppressWarnings({"CloneDoesntCallSuperClone"})
  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public TizenSdkData getState() {
    return this;
  }

  public void loadState(TizenSdkData state) {
    XmlSerializerUtil.copyBean(state, this);
  }

  public String getHomePath() {
    return homePath;
  }

  public String getVersion() {
    return version;
  }

  public String getSdbPath() {
    return sdbPath;
  }

  public String getTizenPath() {
    return tizenPath;
  }
}