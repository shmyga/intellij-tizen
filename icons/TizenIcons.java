package icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class TizenIcons {
  private static Icon load(String path) {
    return IconLoader.getIcon(path, TizenIcons.class);
  }

  public static final Icon Tizen128 = load("/icons/tizen128.png");
  public static final Icon Tizen16 = load("/icons/tizen16.png");
}
